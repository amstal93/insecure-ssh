DOCKER_IMAGE ?= insecure-ssh
DOCKER_TAG   ?= latest

UT_IMAGE ?= gcr.io/gcp-runtimes/container-structure-test
UTFLAGS  ?= --quiet

all local:
	@test -f from/$(DOCKER_FROM_IMAGE)/$(DOCKER_TAG)/Dockerfile || ( \
	  echo "error: 'from/$(DOCKER_FROM_IMAGE)/$(DOCKER_TAG)/Dockerfile' not found." >&2 ; \
	  echo "info: check value of environment variable 'DOCKER_FROM_IMAGE'" >&2 ; \
	  exit 1)
	docker build $(DOCKERFLAGS) --tag $(DOCKER_IMAGE):$(DOCKER_TAG) \
	  -f from/$(DOCKER_FROM_IMAGE)/$(DOCKER_TAG)/Dockerfile .

check: local
	# build a *-check image, no DOCKERFLAGS to avoid possible pull
	docker build --tag $(DOCKER_IMAGE):$(DOCKER_TAG)-check \
	  --build-arg DOCKER_FROM=$(DOCKER_IMAGE):$(DOCKER_TAG) \
	  -f from/$(DOCKER_FROM_IMAGE)/$(DOCKER_TAG)/Dockerfile.check .
	docker pull $(UT_IMAGE)
	docker run --rm \
	  -v /var/run/docker.sock:/var/run/docker.sock \
	  -v "$(CURDIR)/tests:/tests:ro" \
	  $(UT_IMAGE) test $(UTFLAGS) --image $(DOCKER_IMAGE):$(DOCKER_TAG)-check \
	  --config /tests/test.yml
	# remove the *-check image  
	-@docker rmi $(DOCKER_IMAGE):$(DOCKER_TAG)-check

matrix:
	for i in `cd from && find . -name Dockerfile -type f` ; do \
	  path=`dirname $${i#./}` ; \
	  $(MAKE) $(MAKEFLAGS) \
		DOCKER_IMAGE=$${path%/*}-insecure-ssh \
		DOCKER_FROM_IMAGE=$${path%/*} \
		DOCKER_TAG=$${path##*/} \
		local ; \
	done

matrix-check:
	for i in `cd from && find . -name Dockerfile -type f` ; do \
	  path=`dirname $${i#./}` ; \
	  $(MAKE) $(MAKEFLAGS) \
		DOCKER_IMAGE=$${path%/*}-insecure-ssh \
		DOCKER_FROM_IMAGE=$${path%/*} \
		DOCKER_TAG=$${path##*/} \
		check ; \
	done

.DEFAULT_GOAL:= all
.PHONY: all local matrix check matrix-check

