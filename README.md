# Docker insecure-ssh

These docker images contain an openssh-server with login authentication disabled and automatic account creation.

This docker image is willingly insecure by design.

**WARNING** Use at your own risk **WARNING**

## Description

When trying to connect to the openssh-server contained in this docker image:

* If account already exists, 'root' included, and has a valid shell, then access will be granted.

* If account does not exist, then account is created and you can then connect a second time to be automatically granted access.

A use case for this docker image is for unit testing other services that require a ssh server but without adding an ssh private/public key pair to the image.

## Building

The dockerfiles contained in this repository have been designed to be built either by docker hub or by make. They have also been designed in order to build a matrix of images (debian:stretch, debian:stretch-slim, ...).

### Building one image with make

To build locally one image:

~~~bash
# name the destination image:
export DOCKER_IMAGE=debian-insecure-ssh
# name the image to inherit from via path:
export DOCKER_FROM_IMAGE=debian
# name the tag, this will be mapped 1-to-1
export DOCKER_TAG=stretch-slim

# make local image:
make

# test the local image:
make check
~~~

### Building all images

~~~bash
# debian:stretch will be mapped to debian-insecure-ssh:stretch
# etc.
make matrix
~~~

## Source and Image Locations

Because of impossible namespace names (github, docker hub), missing gitlab-dockerhub integration, and the desire for short docker pull links, this repository is:

* Hosted on Gitlab: https://gitlab.com/jlecomte/images/insecure-ssh

* Mirrored onto Github: https://github.com/julien-lecomte/docker-insecure-ssh

  Github is only a mirror and is not monitored for pull requests. Because of
  missing namespaces on Github, 'docker' has been appended to project name as
  a placeholder for the docker inherited image.

* Deployed on Docker hub

  |       **FROM**      | **docker hub**                                                                                               |
  |:-------------------:|--------------------------------------------------------------------------------------------------------------|
  | debian:stretch-slim | [julienlecomte/debian-insecure-ssh](https://hub.docker.com/r/julienlecomte/debian-insecure-ssh):stretch-slim |

## Caveats

* If running the docker image interactively, you can kill the openssh-server with ctrl-$

* If user does not exist, this image requires two separate connections to work. The first connection creates the account, the second succeeds. A possible hack would be creating the user account before sshd calls libpam.

## License

This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/jlecomte/images/insecure-ssh/raw/master/LICENSE) file for details

